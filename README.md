<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="/logo.png" alt="Project logo"></a>
</p>

<h3 align="center">Project Title</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![GitHub Issues](https://img.shields.io/gitlab/issues/open-raw/majksa/merge-request-previews)](https://img.shields.io/gitlab/merge-requests/open/majksa/merge-request-previews)
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/pulls)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center"> Allows you to preview merge requests in a docker container.
    <br> Also comes with a proxy to allow you to preview multiple merge requests at the same time.
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [Authors](#authors)

## 🧐 About <a name = "about"></a>

Only feature that has been missing for me in caprover (compared to coolify.io for example) is the ability to preview merge requests. This project allows you to do that.

## 🚀 Deployment <a name = "deployment"></a>

### Running on caprover

Copy the caprover.yml file to your caprover instance as a one-click-app and deploy it.

### Running using docker (swarm mode required!)

```bash
# First create a docker network for the proxy to be able to communicate with the merge request preview containers
docker network create --driver overlay captain-overlay-network

# Then create a folder for the data to be stored in
mkdir data

# Finally deploy the application
docker run -d \
    -p 80:80 \ # Port for the proxy
    -v "${pwd}/data":/app/data \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -e DOCKER_REGISTRY=registry.gitlab.com \ # Docker registry
    -e DOCKER_USERNAME=majksa \ # Docker registry username
    -e DOCKER_PASSWORD=my_personal_token \ # Docker registry password
    -e WEBHOOK_TOKEN=my_webhook_token \ # Webhook token, save this for later
    majksa/merge-request-previews
```

## 🎈 Usage <a name = "usage"></a>

### Setting up the CI/CD variables

Add the following variables to your gitlab project:
[More info](https://docs.gitlab.com/ee/ci/variables/#variables)

```yaml
URL= # URL where you have deployed the application (e.g. https://merge-requests.example.com)
TOKEN= # Webhook token that you have set during deployment (WEBHOOK_TOKEN)
```

### Setting up pipeline to create the docker images

Add the following to your .gitlab-ci.yml file:

```yaml
build:
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - apt-get update && apt-get install -y curl
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t "$CI_REGISTRY_IMAGE":latest -t "$CI_REGISTRY_IMAGE":"$CI_COMMIT_SHORT_SHA" .
    - docker push "$CI_REGISTRY_IMAGE":latest
    - docker push "$CI_REGISTRY_IMAGE":"$CI_COMMIT_SHORT_SHA"
    - echo "Updating all merge requests previewes for this branch"
    - curl -X POST -H "Content-Type:application/json" -H "X-Gitlab-Token:$TOKEN" -d "{\"branch\":\"$CI_COMMIT_BRANCH\",\"project\":\"$CI_PROJECT_PATH\"}" $URL/mrp/api/update # Update all merge requests previewes for this branch
```

This will build the docker image and push it to your gitlab registry.
It will also trigger the update of all merge requests previews for the current branch.

You can also optionally specify a docker image and/or tag as data of the curl request. By default it will use the latest tag and $CI_REGISTRY_IMAGE.

`curl -X POST -H "Content-Type:application/json" -H "X-Gitlab-Token:$TOKEN" -d "{\"branch\":\"$CI_COMMIT_BRANCH\",\"project\":\"$CI_PROJECT_PATH\",\"tag\":\"latest\",\"image\":\"$CI_REGISTRY_IMAGE\"}" "$URL/mrp/api/update"`

### Adding a webhook to your GitLab project

Go to your project settings and add a webhook that will trigger the pipeline on merge request events with the following settings:

- URL
  URL where you have deployed the application + /mrp/api/update
  e.g. https://merge-requests.example.com/mrp/api/update
  you can optionally add a query parameter to specify the docker image (tag will be the commit short sha) e.g. https://merge-requests.example.com/mrp/api/update?image=registry.gitlab.com/majksa/merge-request-previews

- Secret Token
  Webhook token that you have set during deployment (WEBHOOK_TOKEN)

### Previewing the merge request

Open the URL where you have deployed the application in your browser and provide the project name (with dashed instead of slashes) and merge request id.
e.g. https://merge-requests.example.com/?project=majksa-merge-request-previews&mr=1
will open the merge request with id 1 of the project majksa/merge-request-previews.
This will also create a cookie that will remember the project and merge request id and next time you can open the URL without the query parametes (https://merge-requests.example.com).

## ⛏️ Built Using <a name = "built_using"></a>

- [🦀 Rust](https://www.rust-lang.org/)

## ✍️ Authors <a name = "authors"></a>

- [@majksa](https://github.com/maxa-ondrej) - Idea & Initial work
