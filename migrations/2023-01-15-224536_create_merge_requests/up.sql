-- Your SQL goes here
CREATE TABLE merge_requests
(
    id INTEGER PRIMARY KEY NOT NULL,
    gitlab_id INTEGER UNIQUE NOT NULL,
    project TEXT NOT NULL,
    container TEXT NOT NULL,
    branch TEXT NOT NULL
);
