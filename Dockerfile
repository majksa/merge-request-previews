# Build the binary
FROM rust as build

# create a new empty shell project
RUN USER=root cargo new --bin mr_previews
WORKDIR /mr_previews

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# this build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# copy your source tree
COPY ./migrations ./migrations
COPY ./src ./src

# build for release
RUN rm ./target/release/deps/mr_previews*
RUN cargo build --release

# Run the binary
FROM debian:buster-slim

RUN apt-get update && apt-get install -y \
    libsqlite3-dev nginx \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /app/data; touch /app/data/database.sqlite
WORKDIR /app
RUN mkdir -p /usr/share/nginx/logs/ && touch /usr/share/nginx/logs/access.log && touch /usr/share/nginx/logs/error.log

COPY ./nginx.conf ./proxy.conf /etc/nginx/
COPY --from=build /mr_previews/target/release/mr_previews .

EXPOSE 80

CMD bash -c "nginx; ./mr_previews"
