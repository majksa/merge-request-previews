pub mod models;
pub mod schema;

use diesel::sqlite::SqliteConnection;
use diesel::{prelude::*, sqlite::Sqlite};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use std::env;
use std::error;

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

pub fn establish_connection() -> SqliteConnection {
    let database_url = env::var("DATABASE_URL").unwrap_or("data/database.sqlite".to_string());
    SqliteConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

fn run_migrations(
    connection: &mut impl MigrationHarness<Sqlite>,
) -> Result<(), Box<dyn error::Error + Send + Sync + 'static>> {
    connection.run_pending_migrations(MIGRATIONS)?;

    Ok(())
}

pub fn prepare_db() {
    match establish_connection().transaction::<_, diesel::result::Error, _>(|conn| {
        run_migrations(conn).unwrap();
        Ok(())
    }) {
        Ok(_) => println!("Migrations ran successfully!"),
        Err(e) => panic!("Error running migrations: {}", e),
    }
}
