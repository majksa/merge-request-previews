use crate::db::schema::merge_requests;
use diesel::prelude::*;

#[derive(Queryable, PartialEq, Debug)]
#[diesel(table_name = merge_requests)]
pub struct MergeRequest {
    pub id: i32,
    pub gitlab_id: i32,
    pub project: String,
    pub container: String,
    pub branch: String,
}

#[derive(Insertable)]
#[diesel(table_name = merge_requests)]
pub struct NewMergeRequest {
    pub gitlab_id: i32,
    pub project: String,
    pub container: String,
    pub branch: String,
}

impl NewMergeRequest {
    pub fn new(
        gitlab_id: i32,
        project: String,
        container: String,
        branch: String,
    ) -> NewMergeRequest {
        NewMergeRequest {
            gitlab_id,
            project,
            container,
            branch,
        }
    }
}
