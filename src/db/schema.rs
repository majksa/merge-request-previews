// @generated automatically by Diesel CLI.

diesel::table! {
    merge_requests (id) {
        id -> Integer,
        gitlab_id -> Integer,
        project -> Text,
        container -> Text,
        branch -> Text,
    }
}
