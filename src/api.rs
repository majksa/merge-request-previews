use crate::{
    db::establish_connection,
    service::{handle_commit, handle_merge_requet, PreviewError},
};
use bollard::Docker;
use gitlab::webhooks::MergeRequestHook;
use poem::{handler, http::StatusCode, Body, IntoResponse, Request, Response};
use serde::{Deserialize, Serialize};
use std::env;

fn check_token(req: &Request) -> bool {
    let secret = env::var("WEBHOOK_TOKEN").expect("WEBHOOK_TOKEN must be set");
    match req.header("X-Gitlab-Token") {
        Some(token) => token == secret,
        None => false,
    }
}

#[derive(Debug)]
pub struct ApiResponse {
    message: String,
    status: StatusCode,
}

impl ApiResponse {
    fn new<S: AsRef<str>>(message: S, status: StatusCode) -> ApiResponse {
        ApiResponse {
            message: message.as_ref().to_string(),
            status,
        }
    }

    fn ok<S: AsRef<str>>(message: S) -> ApiResponse {
        ApiResponse::new(message, StatusCode::OK)
    }

    fn created<S: AsRef<str>>(message: S) -> ApiResponse {
        ApiResponse::new(message, StatusCode::CREATED)
    }

    fn unauthorised<S: AsRef<str>>(message: S) -> ApiResponse {
        ApiResponse::new(message, StatusCode::UNAUTHORIZED)
    }

    fn not_found<S: AsRef<str>>(message: S) -> ApiResponse {
        ApiResponse::new(message, StatusCode::NOT_FOUND)
    }

    fn internal() -> ApiResponse {
        ApiResponse::new("Internal server error", StatusCode::INTERNAL_SERVER_ERROR)
    }
}

impl IntoResponse for ApiResponse {
    fn into_response(self) -> Response {
        Response::builder()
            .status(self.status)
            .body(Body::from_string(self.message))
    }
}
#[derive(Deserialize)]
struct WebHookParams {
    image: Option<String>,
}

#[handler]
pub async fn webhook(body: Body, req: &Request) -> ApiResponse {
    if !check_token(req) {
        return ApiResponse::unauthorised("Invalid token!");
    }
    #[cfg(unix)]
    let docker = Docker::connect_with_socket_defaults().unwrap();
    let data: MergeRequestHook = body.into_json().await.unwrap();
    let connection = &mut establish_connection();
    let image = req
        .params::<WebHookParams>()
        .ok()
        .and_then(|params| params.image);
    match handle_merge_requet(&data, connection, &docker, image).await {
        Ok(created) => {
            if created {
                ApiResponse::created("Created!")
            } else {
                ApiResponse::ok("Updated!")
            }
        }
        Err(err) => match err {
            PreviewError::MergeRequestNotFound => ApiResponse::not_found("Merge request not found"),
            PreviewError::Internal => ApiResponse::internal(),
        },
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UpdateData {
    pub branch: String,
    pub project: String,
    pub tag: Option<String>,
    pub image: Option<String>,
}

#[handler]
pub async fn update(body: Body, req: &Request) -> ApiResponse {
    if !check_token(req) {
        return ApiResponse::unauthorised("Invalid token!");
    }
    #[cfg(unix)]
    let docker = Docker::connect_with_socket_defaults().unwrap();
    let data: UpdateData = body.into_json().await.unwrap();
    let connection = &mut establish_connection();
    match handle_commit(&data, connection, &docker).await {
        Ok(count) => ApiResponse::ok(format!("Restarted {} instances!", count)),
        Err(err) => match err {
            PreviewError::MergeRequestNotFound => ApiResponse::not_found("Merge request not found"),
            PreviewError::Internal => ApiResponse::internal(),
        },
    }
}
