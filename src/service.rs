use crate::api::UpdateData;
use crate::db::models::MergeRequest;
use crate::db::schema::merge_requests::dsl::*;
use crate::db::{models::*, schema::merge_requests};
use crate::docker::{create_credentials, stop_service};
use crate::docker::{create_service, parse_image};
use crate::docker::{parse_container_name, restart_service};
use bollard::Docker;
use diesel::prelude::*;
use diesel::SqliteConnection;
use gitlab::webhooks::MergeRequestHook;

pub enum PreviewError {
    MergeRequestNotFound,
    Internal,
}

impl From<diesel::result::Error> for PreviewError {
    fn from(e: diesel::result::Error) -> Self {
        println!("{}", e);
        PreviewError::Internal
    }
}

impl From<bollard::errors::Error> for PreviewError {
    fn from(e: bollard::errors::Error) -> Self {
        println!("{}", e);
        PreviewError::Internal
    }
}

fn get_merge_request(
    conn: &mut SqliteConnection,
    mr_id: i32,
    project_name: &str,
) -> Result<Option<MergeRequest>, PreviewError> {
    Ok(merge_requests
        .filter(gitlab_id.eq(mr_id))
        .filter(project.eq(String::from(project_name)))
        .first::<MergeRequest>(conn)
        .optional()?)
}

fn get_merge_requests(
    conn: &mut SqliteConnection,
    branch_name: &str,
    project_name: &str,
) -> Result<Vec<MergeRequest>, PreviewError> {
    Ok(merge_requests
        .filter(branch.eq(branch_name))
        .filter(project.eq(String::from(project_name)))
        .load::<MergeRequest>(conn)?)
}

pub async fn handle_merge_requet(
    data: &MergeRequestHook,
    conn: &mut SqliteConnection,
    docker: &Docker,
    image_force: Option<String>,
) -> Result<bool, PreviewError> {
    let container_name: &String = &parse_container_name(
        &data.project.path_with_namespace,
        data.object_attributes.iid.value() as usize,
    );
    let tag = &match &data.object_attributes.merge_commit_sha {
        Some(sha) => sha.value().clone().chars().take(8).collect::<String>(),
        None => "latest".to_string(),
    };
    let image = match image_force {
        Some(img) => format!("{}:{}", img, tag),
        None => parse_image(&data.project.path_with_namespace, tag),
    };
    let credentials = Some(create_credentials());
    let mr_id = data.object_attributes.iid.value() as i32;
    let name = &data.project.path_with_namespace;
    let source_branch = &data.object_attributes.source_branch;
    let stop = match data.object_attributes.state {
        gitlab::MergeRequestState::Closed => true,
        gitlab::MergeRequestState::Merged => true,
        _ => false,
    };

    match get_merge_request(conn, mr_id, name)? {
        Some(db_data) => {
            if stop {
                stop_service(&docker, db_data.container.clone()).await?
            } else {
                let db_container: &String = &db_data.container;
                if docker.inspect_service(db_container, None).await.is_ok() {
                    restart_service(
                        &docker,
                        db_container.clone(),
                        container_name.clone(),
                        image,
                        credentials,
                    )
                    .await?;
                } else {
                    create_service(&docker, container_name.clone(), image, credentials).await?;
                }
                diesel::update(merge_requests::table)
                    .filter(id.eq(db_data.id))
                    .set(container.eq(container_name))
                    .execute(conn)?;
            }
            Ok(false)
        }
        None => {
            if stop {
                Err(PreviewError::MergeRequestNotFound)
            } else {
                create_service(&docker, container_name.clone(), image, credentials).await?;
                diesel::insert_into(merge_requests::table)
                    .values(NewMergeRequest::new(
                        mr_id,
                        name.clone(),
                        container_name.clone(),
                        source_branch.clone(),
                    ))
                    .execute(conn)?;
                Ok(true)
            }
        }
    }
}

pub async fn handle_commit(
    data: &UpdateData,
    conn: &mut SqliteConnection,
    docker: &Docker,
) -> Result<usize, PreviewError> {
    let tag = match &data.tag {
        Some(tag) => tag,
        None => "latest",
    };
    let image = match data.image {
        Some(ref img) => format!("{}:{}", img, tag),
        None => parse_image(&data.project, tag),
    };
    let mut threads: Vec<_> = vec![];
    for mr in get_merge_requests(conn, &data.branch, &data.project)?.iter() {
        let old_container_name: &String = &mr.container;
        let container_name: &String = &parse_container_name(&data.project, mr.gitlab_id as usize);
        let credentials = Some(create_credentials());
        if docker.inspect_service(container_name, None).await.is_ok() {
            threads.push(restart_service(
                &docker,
                old_container_name.clone(),
                container_name.clone(),
                image.clone(),
                credentials,
            ));
        }
    }
    let mut count: usize = 0;
    for thread in threads {
        thread.await?;
        count += 1;
    }
    Ok(count)
}
