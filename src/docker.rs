use bollard::{
    auth::DockerCredentials,
    models::ServiceSpec,
    service::{
        NetworkAttachmentConfig, ServiceCreateResponse, ServiceUpdateResponse, TaskSpec,
        TaskSpecContainerSpec, UpdateServiceOptions,
    },
    Docker,
};
use std::env;

pub fn parse_container_name(project: &str, mr_id: usize) -> String {
    format!("gitlab--{}--{}", project.replace("/", "-"), mr_id)
}

pub fn parse_image(project: &String, tag: &str) -> String {
    format!(
        "{}/{}:{}",
        env::var("DOCKER_REGISTRY")
            .ok()
            .unwrap_or("registry.gitlab.com".to_string()),
        project,
        tag
    )
}

pub fn create_credentials() -> DockerCredentials {
    DockerCredentials {
        serveraddress: env::var("DOCKER_REGISTRY").ok(),
        username: env::var("DOCKER_USERNAME").ok(),
        password: env::var("DOCKER_PASSWORD").ok(),
        ..Default::default()
    }
}

pub async fn create_service(
    docker: &Docker,
    name: String,
    image: String,
    credentials: Option<DockerCredentials>,
) -> Result<ServiceCreateResponse, bollard::errors::Error> {
    let service = ServiceSpec {
        name: Some(name),
        networks: Some(vec![NetworkAttachmentConfig {
            target: Some("captain-overlay-network".to_string()),
            ..Default::default()
        }]),
        task_template: Some(TaskSpec {
            container_spec: Some(TaskSpecContainerSpec {
                image: Some(image),
                ..Default::default()
            }),
            ..Default::default()
        }),
        ..Default::default()
    };
    docker.create_service(service, credentials).await
}

pub async fn restart_service(
    docker: &Docker,
    id: String,
    name: String,
    image: String,
    credentials: Option<DockerCredentials>,
) -> Result<ServiceUpdateResponse, bollard::errors::Error> {
    let service = ServiceSpec {
        name: Some(name),
        task_template: Some(TaskSpec {
            container_spec: Some(TaskSpecContainerSpec {
                image: Some(image),
                ..Default::default()
            }),
            ..Default::default()
        }),
        ..Default::default()
    };
    let options = UpdateServiceOptions {
        ..Default::default()
    };
    docker
        .update_service(id.as_str(), service, options, credentials)
        .await
}

pub async fn stop_service(docker: &Docker, id: String) -> Result<(), bollard::errors::Error> {
    docker.delete_service(id.as_str()).await
}
