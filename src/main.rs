pub mod api;
pub mod db;
pub mod docker;
pub mod service;

use crate::db::prepare_db;
use api::{update, webhook};
use dotenvy::dotenv;
use poem::listener::TcpListener;
use poem::middleware::Tracing;
use poem::{post, EndpointExt, Route, Server};

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    if std::env::var_os("RUST_LOG").is_none() {
        std::env::set_var("RUST_LOG", "poem=debug");
    }
    dotenv().ok();

    prepare_db();
    let app = Route::new()
        .at("/webhook", post(webhook))
        .at("/update", post(update))
        .with(Tracing);
    Server::new(TcpListener::bind("0.0.0.0:3000"))
        .name("merge-request-previews")
        .run(app)
        .await
}
